function forward_euler(μ, σ, f, X_0, T; Δt=1e-8, a=-Inf, b=Inf)
  # Number of steps to simulate process from time 0 to T
  num_steps = trunc(Int64, T/Δt)
  # Allocate a vector to store the path
  X = zeros(num_steps)
  # Start process at X_0
  X[1] = X_0
  # Simulate process through time
  for i=2:num_steps
    # Take Euler-Maruyama step
    X[i] = X[i-1] + μ(X[i-1])*Δt + σ(X[i-1])*sqrt(Δt)*randn()
    # Return early terminated path if boundary is hit
    if X[i] <= a || X[i] >= b
      return X[1:i]
    end
  end
  return X
end

function trapezoid_integrate(X, V; Δt=1e-8)
  # compute ∫V(X_s)ds using the trapezoidal rule
  return sum([
    0.5*(V(X[i-1]) + V(X[i]))*Δt for i=2:length(X)
    ])
end

function feynman_kac(μ, σ, V, f, X_0, T; g=x->0, Δt=1e-8, a=-Inf, b=Inf, n=1000)
  out = 0.0
  for _=1:n
    X = forward_euler(μ, σ, f, X_0, T, Δt=Δt, a=a, b=b)
    intV = trapezoid_integrate(X, V, Δt=Δt)
    if X[end] <= a || X[end] >= b
      out += g(X[end]) * exp(intV)
    else
      out += f(X[end]) * exp(intV)
    end
  end
  return out/n
end
