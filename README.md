# **Solving Diffusion Equations**

Diffusions are a common class of parabolic partial differential equations.

We will focus in particular on equations of the form
```math
\frac{\partial u}{\partial t} + \mu^\top \nabla u + \frac{1}{2} \text{tr}\Big( \sigma \sigma^\top \nabla \nabla^\top u \Big) + Vu = 0
```
where the functions $`\mu:\mathbb{R}^n \to \mathbb{R}^n, \sigma:\mathbb{R}^n \to \mathbb{R}^n`$, and $`V:\mathbb{R}^n \to \mathbb{R}`$ are referred to as the _drift_, _diffusion_, and _potential_.

<!-- $$ \frac{\partial u}{\partial t} + \mu \frac{\partial u}{\partial x} + \frac{1}{2} \sigma^2 \frac{\partial^2 u}{\partial x^2} + Vu = 0 $$ -->

Typically, faced with a deterministic PDE without a closed-form solution, one would try constructing some sort of grid in the domain and solve the resulting discretized problem. Instead, we can discretize a stochastic partial differential equation in time using a forward Euler scheme.

```julia
function forward_euler(μ, σ, f, X_0, T; Δt=1e-8, a=-Inf, b=Inf)
  # Number of steps to simulate process from time 0 to T
  num_steps = trunc(Int64, T/Δt)
  # Allocate a vector to store the path
  X = zeros(num_steps)
  # Start process at X_0
  X[1] = X_0
  # Simulate process through time
  for i=2:num_steps
    # Take Euler-Maruyama step
    X[i] = X[i-1] + μ(X[i-1])*Δt + σ(X[i-1])*sqrt(Δt)*randn()
    # Return early terminated path if boundary is hit
    if X[i] <= a || X[i] >= b
      return X[1:i]
    end
  end
  return X
end

```

## Stochastic Partial Differential Equations

### Brownian Motion

### Infinitesimal Generators

### Itô Calculus

### The Feynman-Kac Formula

### Euler-Maruyama Discretization

## Finite Differences

## Finite Elements
